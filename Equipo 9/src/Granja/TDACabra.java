package Granja;

import java.util.ArrayList;

public class TDACabra {

    private ArrayList<Cabra> cabras = new ArrayList<>();
    private int year;

    public TDACabra(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void addCabra(Cabra cabra) {
        cabras.add(cabra);
    }

    public boolean hayCabras() {
        return cabras.size() != 0;
    }

    private int getTotalLeche(ArrayList<Integer> leche) {
        int suma = 0;
        for (int i = 0; i < leche.size(); i++) {
            suma += leche.get(i);
        }
        return suma;
    }

    private int getPromedioLeche(ArrayList<Integer> leche) {
        int suma = 0;
        for (int i = 0; i < leche.size(); i++) {
            suma += leche.get(i);
        }
        return suma / leche.size();
    }

    public String getLechePorCabra() {
        if (hayCabras()) {
            String msg = "";
            for (int i = 0; i < cabras.size(); i++) {
                msg += "Cabra - " + cabras.get(i).getRegistroPropiedad() + " | Leche producida: " + getTotalLeche(cabras.get(i).getLeche()) + "L\n";
            }
            return msg;
        }
        return "No hay cabras";
    }

    public String getPromedioLechePorCabra() {
        if (hayCabras()) {
            String msg = "";
            for (int i = 0; i < cabras.size(); i++) {
                msg += "Cabra - " + cabras.get(i).getRegistroPropiedad() + " | Promedio leche/mes: " + getPromedioLeche(cabras.get(i).getLeche()) + "L\n";
            }
            return msg;
        }
        return "No hay cabras";
    }

    public String getMAXLeche() {
        if (hayCabras()) {
            ArrayList<Cabra> maximas = new ArrayList<>();
            int max = getTotalLeche(cabras.get(0).getLeche());
            for (int i = 1; i < cabras.size(); i++) {
                Cabra cabra = cabras.get(i);
                if (getTotalLeche(cabra.getLeche()) >= max) {
                    max = getTotalLeche(cabra.getLeche());
                    if (!maximas.contains(cabra)) {
                        maximas.add(cabra);
                    }
                }
            }

            if (getTotalLeche(cabras.get(0).getLeche()) >= max) {
                maximas.add(cabras.get(0));
            }

            String msg = "";
            for (int i = 0; i < maximas.size(); i++) {
                msg += maximas.get(i).toString() + "\n";
            }
            return msg;
        }
        return "No hay cabras";
    }

    public String getPromedioRaza() {
        if (hayCabras()) {
            ArrayList<String> razas = new ArrayList<>();
            for (int i = 0; i < cabras.size(); i++) {
                if (!razas.contains(cabras.get(i).getRaza())) {
                    razas.add(cabras.get(i).getRaza());
                }
            }

            String msg = "";
            for (int i = 0; i < razas.size(); i++) {
                msg += " || Raza: " + razas.get(i) + "\n";
                int suma = 0;
                int count = 0;
                for (int j = 0; j < cabras.size(); j++) {
                    if (cabras.get(j).getRaza().equals(razas.get(i))) {
                        suma += getTotalLeche(cabras.get(j).getLeche());
                        count++;
                    }
                }
                msg += "    Promedio de leche: " + (suma / count) + "\n";
            }
            return msg;
        }
        return "No hay cabras";
    }
    
}
