package Granja;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static TDAVaca getTDAVacaYear(ArrayList<TDAVaca> tda, int year) {
        TDAVaca retorno = null;
        for (int i = 0; i < tda.size(); i++) {
            if (tda.get(i).getYear() == year) {
                retorno = tda.get(i);
                break;
            }
        }
        return retorno;
    }

    public static TDACabra getTDACabraYear(ArrayList<TDACabra> tda, int year) {
        TDACabra retorno = null;
        for (int i = 0; i < tda.size(); i++) {
            if (tda.get(i).getYear() == year) {
                retorno = tda.get(i);
                break;
            }
        }
        return retorno;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<TDAVaca> tdaVacas = new ArrayList<>();
        ArrayList<TDACabra> tdaCabras = new ArrayList<>();
        boolean flag = true;

        while(flag) {
            System.out.println("Menu Principal | Escoge una opcion");
            System.out.println("1. Agregar vaca");
            System.out.println("2. Total de leche de cada vaca");
            System.out.println("3. Promedio semanal por vaca");
            System.out.println("4. Vacas con mayor produccion semanal");
            System.out.println("5. Promedio semanal de las vacas por raza");
            System.out.println("6. Agregar cabra");
            System.out.println("7. Total de leche de cada cabra");
            System.out.println("8. Promedio semanal por cabra");
            System.out.println("9. Cabras con mayor produccion semanal");
            System.out.println("10. Promedio semanal de las cabras por raza");
            System.out.println("Otro. Salir");
            switch (sc.nextInt()) {
                case 1:
                    System.out.println("\nIngresa el a�o de produccion");
                    int year = sc.nextInt();

                    TDAVaca tda = getTDAVacaYear(tdaVacas, year);
                    if (tda == null) {
                        tda = new TDAVaca(year);
                        tdaVacas.add(tda);
                    }

                    Vaca vaca = new Vaca();
                    System.out.println("Ingresa el registro de propiedad (8 digitos)"); sc.nextLine();
                    vaca.setRegistroPropiedad(sc.nextLine());
                    System.out.println("Ingresa su fecha de adquisicion");
                    vaca.setFechaAdquisicion(sc.nextLine());
                    System.out.println("Ingresa su edad");
                    vaca.setEdad(sc.nextInt());
                    System.out.println("Ingresa su raza"); sc.nextLine();
                    vaca.setRaza(sc.nextLine());

                    System.out.println("�Cuantas semanas de produccion se van a registrar?");
                    int semanas = sc.nextInt();
                    for (int i = 0; i < semanas; i++) {
                        System.out.println("Ingresa la produccion de la semana " + (i + 1));
                        vaca.addLeche(sc.nextInt());
                    }

                    tda.addVaca(vaca);
                    System.out.println(">> Vaca agregada con Exito <<\n");
                    break;

                case 2:
                    if (tdaVacas.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaVacas.size(); i++) {
                            System.out.println(">> Año: " + tdaVacas.get(i).getYear());
                            System.out.println(tdaVacas.get(i).getLechePorVaca());
                        }

                    }
                    break;

                case 3:
                    if (tdaVacas.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaVacas.size(); i++) {
                            System.out.println(">> A�o: " + tdaVacas.get(i).getYear());
                            System.out.println(tdaVacas.get(i).getPromedioLechePorVaca());
                        }

                    }
                    break;

                case 4:
                    if (tdaVacas.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaVacas.size(); i++) {
                            System.out.println(">> A�o: " + tdaVacas.get(i).getYear());
                            System.out.println(tdaVacas.get(i).getMAXLeche());
                        }

                    }
                    break;

                case 5:
                    if (tdaVacas.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaVacas.size(); i++) {
                            System.out.println(tdaVacas.get(i).getPromedioRaza());
                        }
                    }
                    break;

                case 6:
                    System.out.println("\nIngresa el a�o de produccion");
                    int yearC = sc.nextInt();

                    TDACabra tdaC = getTDACabraYear(tdaCabras, yearC);
                    if (tdaC == null) {
                        tdaC = new TDACabra(yearC);
                        tdaCabras.add(tdaC);
                    }

                    Cabra cabra = new Cabra();
                    System.out.println("Ingresa el registro de propiedad (8 digitos)"); sc.nextLine();
                    cabra.setRegistroPropiedad(sc.nextLine());
                    System.out.println("Ingresa su fecha de adquisicion");
                    cabra.setFechaAdquisicion(sc.nextLine());
                    System.out.println("Ingresa su edad");
                    cabra.setEdad(sc.nextInt());
                    System.out.println("Ingresa su raza"); sc.nextLine();
                    cabra.setRaza(sc.nextLine());

                    System.out.println("�Cuantas semanas de produccion se van a registrar?");
                    int semanasC = sc.nextInt();
                    for (int i = 0; i < semanasC; i++) {
                        System.out.println("Ingresa la produccion de la semana " + (i + 1));
                        cabra.addLeche(sc.nextInt());
                    }

                    tdaC.addCabra(cabra);
                    System.out.println(">> Vaca agregada con Exito <<\n");
                    break;

                case 7:
                    if (tdaCabras.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaCabras.size(); i++) {
                            System.out.println(">> A�o: " + tdaCabras.get(i).getYear());
                            System.out.println(tdaCabras.get(i).getLechePorCabra());
                        }

                    }
                    break;

                case 8:
                    if (tdaCabras.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaCabras.size(); i++) {
                            System.out.println(">> Año: " + tdaCabras.get(i).getYear());
                            System.out.println(tdaCabras.get(i).getPromedioLechePorCabra());
                        }

                    }
                    break;

                case 9:
                    if (tdaCabras.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaCabras.size(); i++) {
                            System.out.println(">> A�o: " + tdaCabras.get(i).getYear());
                            System.out.println(tdaCabras.get(i).getMAXLeche());
                        }

                    }
                    break;

                case 10:
                    if (tdaCabras.isEmpty()) {
                        System.out.println("No hay vacas\n");
                    } else {
                        for (int i = 0; i < tdaCabras.size(); i++) {
                            System.out.println(tdaCabras.get(i).getPromedioRaza());
                        }
                    }
                    break;
            
                default:
                    flag = false;
                    break;
            }
        }
        sc.close();
    }
}
