package Granja;

import java.util.ArrayList;

public class TDAVaca {

    private ArrayList<Vaca> vacas = new ArrayList<>();
    private int year;

    public TDAVaca(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void addVaca(Vaca vaca) {
        vacas.add(vaca);
    }

    public boolean hayVacas() {
        return vacas.size() != 0;
    }

    private int getTotalLeche(ArrayList<Integer> leche) {
        int suma = 0;
        for (int i = 0; i < leche.size(); i++) {
            suma += leche.get(i);
        }
        return suma;
    }

    private int getPromedioLeche(ArrayList<Integer> leche) {
        int suma = 0;
        for (int i = 0; i < leche.size(); i++) {
            suma += leche.get(i);
        }
        return suma / leche.size();
    }

    public String getLechePorVaca() {
        if (hayVacas()) {
            String msg = "";
            for (int i = 0; i < vacas.size(); i++) {
                msg += "Vaca - " + vacas.get(i).getRegistroPropiedad() + " | Leche producida: " + getTotalLeche(vacas.get(i).getLeche()) + "L\n";
            }
            return msg;
        }
        return "No hay vacas";
    }

    public String getPromedioLechePorVaca() {
        if (hayVacas()) {
            String msg = "";
            for (int i = 0; i < vacas.size(); i++) {
                msg += "Vaca - " + vacas.get(i).getRegistroPropiedad() + " | Promedio leche/semana: " + getPromedioLeche(vacas.get(i).getLeche()) + "L\n";
            }
            return msg;
        }
        return "No hay vacas";
    }

    public String getMAXLeche() {
        if (hayVacas()) {
            ArrayList<Vaca> maximas = new ArrayList<>();
            int max = getTotalLeche(vacas.get(0).getLeche());
            for (int i = 1; i < vacas.size(); i++) {
                Vaca vaca = vacas.get(i);
                if (getTotalLeche(vaca.getLeche()) >= max) {
                    max = getTotalLeche(vaca.getLeche());
                    if (!maximas.contains(vaca)) {
                        maximas.add(vaca);
                    }
                }
            }

            if (getTotalLeche(vacas.get(0).getLeche()) >= max) {
                maximas.add(vacas.get(0));
            }

            String msg = "";
            for (int i = 0; i < maximas.size(); i++) {
                msg += maximas.get(i).toString() + "\n";
            }
            return msg;
        }
        return "No hay vacas";
    }

    public String getPromedioRaza() {
        if (hayVacas()) {
            ArrayList<String> razas = new ArrayList<>();
            for (int i = 0; i < vacas.size(); i++) {
                if (!razas.contains(vacas.get(i).getRaza())) {
                    razas.add(vacas.get(i).getRaza());
                }
            }

            String msg = "";
            for (int i = 0; i < razas.size(); i++) {
                msg += " || Raza: " + razas.get(i) + "\n";
                int suma = 0;
                int count = 0;
                for (int j = 0; j < vacas.size(); j++) {
                    if (vacas.get(j).getRaza().equals(razas.get(i))) {
                        suma += getTotalLeche(vacas.get(j).getLeche());
                        count++;
                    }
                }
                msg += "    Promedio de leche: " + (suma / count) + "\n";
            }
            return msg;
        }
        return "No hay vacas";
    }
}