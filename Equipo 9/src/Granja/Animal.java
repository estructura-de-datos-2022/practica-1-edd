package Granja;

public class Animal {
    
    private String registroPropiedad;
    private String fechaAdquisicion;
    private int edad;
    private String raza;

    public Animal() {
        this(null, null, -1, null);
    }

    public Animal(String registroPropiedad, String fechaAdquisicion, int edad, String raza) {
        this.registroPropiedad = registroPropiedad;
        this.fechaAdquisicion = fechaAdquisicion;
        this.edad = edad;
        this.raza = raza;
    }

    public String getRegistroPropiedad() {
        return registroPropiedad;
    }

    public void setRegistroPropiedad(String registroPropiedad) {
        this.registroPropiedad = registroPropiedad;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }
}
