package Granja;

import java.util.ArrayList;

public class Cabra extends Animal {

    private ArrayList<Integer> leche = new ArrayList<>();

    public ArrayList<Integer> getLeche() {
        return leche;
    }

    public void addLeche(int leche) {
        this.leche.add(leche);
    }

    @Override
    public String toString() {
        String msg = "[Cabra - RP: " + getRegistroPropiedad() + ", FA: " + getFechaAdquisicion() + ", Edad: " + getEdad() + ", Raza: " + getRaza() + "\n";
        for (int i = 0; i < leche.size(); i++) {
            msg += "Mes " + (i + 1) + ": " + leche + "L\n";
        }
        return msg + "]";
    }
}
